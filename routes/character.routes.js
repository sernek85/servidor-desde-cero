const express = require('express')
const Character = require('../models/Character')

const router = express.Router()

router.get('/', async (res,req)=>{
    try{
        const character = await Character.find()
        return res.status(200)
    }
    catch(error){
       return res.status(500).json(error)

    }
})

router.get('/:id', async(res,req)=>{
    try{
        const id = req.params.idç
        const character = await Character.findById(id)
    
        if(character){
            return res.status(200).json(character)
        }else{
            return res.status(404).json('no se encuentra el personaje que buscas')
        }
    }
    catch(error){
        return res.status(404).json(error)
    }
  
})

module.exports = router