const express = require('express')
const indexRouter = require('./routes/index.routes');
const characterRouter = require('./routes/character.routes');


const { connectToDb } = require('./config/db');
connectToDb();

const PORT = 3000;
const server = express();

server.use('/', indexRouter);
server.use('/characters', characterRouter);

const Callback = () => {
    console.log(`Servidor arrancado en http://localhost:${PORT}`);
}

server.listen(PORT, Callback);






























/* 
const require= require('express')

const server = express()

const PORT = 3000

server.use('/',(res,req)=>{
res.send(console.log('el servidor esta funcionando con normalidad'))
})

const callback = ()=>{
    console.log(`servidor arrancado en http://localhost:${PORT}`)
}


server.listen(PORT,callback)


*/