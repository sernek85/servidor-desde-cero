const mongoose = require('mongoose')
const Character = require('../models/Character')
const {DB_URL,CONFIG_DB} = require('../config/db')


const charactersArray = [
    {
        name:'Bruce Wayne',
        age:45,
        alias:'Batman',
        hability:'Rich',
        isSuperHero:true,
        appears:['comic','tv','movie']
    },
    {
        name:'Clark Kent',
        age:35,
        alias:'Superman',
        hability:'fuerza y vuela',
        isSuperHero:true,
        appears:['movie','comic'],
    },
    {
        name:'Logan',
        age:850,
        alias:'Lobezno',
        hability:'fuerza y inmunidad',
        isSuperHero:true,
        appears:['movie','comic','tv']
    },
    {
        name:'Sergio',
        age:36,
        alias:'Standford',
        hability:'perseverancia y optimismo',
        isSuperHero:false,
        appears:['tv']
    },
    {
        name:'Steve',
        age:22,
        alias:'Ciclope',
        hability:'rayo laser',
        isSuperHero:true,
        appears:['movie','comic',]
    },
]

//nos conectamos
//buscamos si existen personajes 
//si no existen los creamos
//si existen borramos coleccion y luego añadimos el character array
//avisamos al usuario si se han añadidio personajes o hubo un error
//cerraremos la conexion con la bbdd

mongoose.connect(DB_URL,CONFIG_DB)
.then(async()=>{
    const allCharacters = await Character.find()

    if(allCharacters.length){
        await Character.collection.drop()
        console.log('coleccion de caracteres eliminada con exito');
    }
})
.catch((error)=>{console.log('eroor buscado en la bbdd')})
.then(async()=>{
    await Character.insertMany(charactersArray)
    console.log('elementos insertados');

})
.catch((error)=>{console.log('error al añadir los personajes');})
.finally(()=>{mongoose.disconnect()})