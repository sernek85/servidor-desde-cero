

//requerimos mongoose
const mongoose = require('mongoose')
//schema es una clase
const Schema = mongoose.Schema

//creamos el esquema de personajes

const characterSchema = new Schema(//la clase schema tiene dos argumentos el primero es el fomrmato q guardamos en la bbdd y el segundo las opciones 
    {
        name:{type:String, required: true },//la prop required hace que el campo sea obligatorio
        age:{type:Number},
        alias:{type:String, required: true },
        hability:{ type:String, },
        isSuperHero:{ type:Boolean, required: true ,default:false},
        level:{type:Number,required: true ,default:0 },
        appears:[{type:String, enum:['tv','comic','movie'], required:true}]
    },
    {
        timestamps :true,// esta propiedad servira para guardar las fechas de realizacion y actualizacion de documentos
    },
)

//creamos y exportamos el modelo Character

const Character = mongoose.model('Characters',characterSchema)//el primer argumento es el nombre de nuestra bbdd,el segundo el esquema


module.exports = Character