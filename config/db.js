const mongoose = require('mongoose')

const DB_URL ='mongodb://localhost:27017/mongo-proyect-first'
const CONFIG_DB = {useNewUrlParser: true, useUnifiedTopology:true,}

//frroma de conectar con la base de datos con pormesas
/* mongoose
.connect(DB_URL,{
    useNewUrlParser: true,
    useUnifiedTopology:true,
})
.then((response)=>{

    const {host , port, name} = response.connection
    
    console.log(`Conectado a ${name} en ${host} :${port}`);
})
.catch((err)=>console.log('Error conectado a la DB',err)) */

//tyr catch // async away
  

//refactorizacion de codigo

const connectToDb = async ()=>{
    try{
    //cuando trabajamos con async awayt la unica forma de poner un catch
    //es con try catch
    const response = await mongoose.connect(DB_URL,CONFIG_DB)
    const {host , port, name} = response.connection
    console.log(`Conectado a ${name} en ${host} :${port}`);
    }
    catch(error){
        console.log('Error conectado a la DB',err)
    }

}

module.exports = {DB_URL,CONFIG_DB,connectToDb}